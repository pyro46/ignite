yesno(){
	COUNT=0
	LIMIT=3
	while [ $COUNT -lt $LIMIT ]; do
	    read -p "Do you wish to install this program?" yn
	    case $yn in
        	[Yy]* ) return 1; break;;
	        [Nn]* ) return 0; break;;
	        * ) echo "Please answer yes or no.";;
	    esac
		COUNT="$(( $COUNT + 1))"
	done
}

yesno 
if [ $? -eq 1 ];
then 
	echo "Yes"
else
	echo "No"
fi
